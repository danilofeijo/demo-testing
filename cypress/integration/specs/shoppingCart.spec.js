/// <reference types='cypress' />

describe('Describe 01', () => {
  beforeEach(() => {
    cy.visit('')
    cy.get('.login').click()

    cy.get('#email').type('karolrendon@test.com')
    cy.get('#passwd').type('Test;123')
    cy.get('#SubmitLogin').click()

    cy.visit('')
  });

  it('Should add a product in the shopping cart', () => {
    // Actions
    cy.contains('Printed Summer Dress').click()
    cy.get('button.add_to_cart').click()
    cy.get('a[title="Proceed to checkout"]').click()

    // Validations
    cy.get('.order_delivery ul.first_item h3')
      .should('contain.text', 'Delivery address')

    cy.get('.order_delivery ul.last_item h3')
      .should('contain.text', 'Invoice adress')

    cy.get('p.cart_navigation')
      .find('a[title="Proceed to checkout"]')
      .should('have.attr', 'href', 'http://automationpractice.com/index.php?controller=order&step=1')
      .find('p')
      .should('have.text', 'Proceed to checkout')
  });
});
